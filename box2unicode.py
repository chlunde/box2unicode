#!/usr/bin/env python
# -*- encoding: utf-8 -*-

# http://xahlee.info/comp/unicode_drawing_shapes.html

rules = [
    [
        "┼",
        """
        ?|?
        -+-
        ?|?
        """
    ],
    [
        "├",
        """
        ?|?
        X+-
        ?|?
        """
    ],
    [
        "┤",
        """
        ?|?
        -+X
        ?|?
        """
    ],
    [
        "┌",
        """
        ?X?
        X+-
        ?|?
        """
    ],
    [
        "┐",
        """
        ?X?
        -+X
        ?|?
        """
    ],
    [
        "└",
        """
        ?|?
        X+-
        ?X?
        """
    ],
    [
        "┘",
        """
        ?|?
        -+X
        ?X?
        """
    ],
    [
        "╭",
        """
        ?X?
        X/-
        ?|?
        """
    ],
    [
        "╮",
        """
        ?X?
        -\\X
        ?|?
        """
    ],
    [
        "╰",
        """
        ?|?
        X\\-
        ?X?
        """
    ],
    [
        "╯",
        """
        ?|?
        -/X
        ?X?
        """
    ],
    [
        "┴",
        """
        ?|?
        -+-
        ?X?
        """
    ],
    [
        "┬",
        """
        ?X?
        -+-
        ?|?
        """
    ],
    [
        "─",
        """
        ?-?
        """
    ],
    [
        "│",
        """
        ???
        ?|?
        ???
        """
    ],
    # [
    #     "╱",
    #     """
    #     ??/
    #     ?/?
    #     /??
    #     """
    # ],
    [
        "►",
        """
        ???
        ->?
        ???
        """
    ],
    [
        "◀",
        """
        ???
        ?<-
        ???
        """
    ],
]

def pattern2matrix(pattern):
    lines = pattern.splitlines()
    lines = lines[1:]
    indent = 0
    for ch in lines[0]:
        if ch == ' ':
            indent += 1
        else:
            break

    lines = [l[indent:] for l in lines]
    if lines[len(lines)-1] == '':
        lines = lines[:-1]
    return lines


def match(rule, matrix, x, y):
    outch, patstring = rule
    pat = pattern2matrix(patstring)
    cx, cy = len(pat[0])/2, len(pat)/2
    cch = pat[cy][cx]
    if matrix[y][x] != cch:
        return None

    for dy in range(-cy, cy+1):
        for dx in range(-cx, cx+1):
            try:
                val = matrix[y+dy][x+dx]
            except:
                val = " "
            try:
                pcode = pat[cy+dy][cx+dx]
            except:
                continue

            if pcode == '?':
                continue
            if pcode == 'X' and val in '-|+/\\':
                return None
            if pcode in '-|+' and val not in "-|+/\\":
                return None

    if False:
        print
        print "----------------------------------"
        for dy in range(-cy, cy+1):
            if y+dy >= len(matrix) or y+dy < 0:
                continue
            print "%s." % (matrix[y+dy][max(0,x-cx):x+cy+1])
        print
        print "\n".join(pat)
        print
        print cch, "-->", outch
        print "----------------------------------"

    return outch


def boxer(s):
    matrix = s.splitlines()
    out = []
    for y, row in enumerate(matrix):
        outrow = ""
        for x, char in enumerate(row):
            ch = None
            for rule in rules:
                ch = match(rule, matrix, x, y)
                if ch is not None:
                    break

            if ch is None:
                ch = char
            outrow += ch
        out.append(outrow)

    print "\n".join(out)

if __name__ == '__main__':
    import sys
    data = sys.stdin.read()
    boxer(data)
